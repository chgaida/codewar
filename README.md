![Panda Logo](./assets/logo.gif)

Just another Drug War/Dope Wars clone. This time the action doesn't take place in New York, but in the world of William Gibson's Neuromancer trilogy and you are a burned-out console cowboy on your way to new fame and fortune.

[![Pipeline Status][pipeline-badge]][pipeline-url]
[![Pylint][pylint-badge]][pylint-url]
[![coverage report][cover-badge]][cover-url]
[![Codestyle: black][codestyle-shield]][codestyle-url]
[![python version][python-version-shield]][python-version-url]

# Table of Contents
* [About](#about)
* [Getting Started](#start)
  - [Installation](#installation)
  - [Start the game](#starting)
* [Contributing](#contrib)
* [License](#license)
* [Contact](#contact)
* [Acknowledgements](#acknowledgements)

<a name="about"></a>
## About
C0D3 WAR is a turned-based strategy game. The aim is to make as much money as possible by traveling from place to place and trading in different items. Each trip is a day/round and a game lasts exactly 30 days/rounds.

<a name="start"></a>
## Getting Started
To get started follow these simple steps.

<a name="installation"></a>
### Installation
First clone the repository:
```sh
git clone https://gitlab.com/chgaida/codewar.git
```
Create and activate a virtual environment (recommended):
```sh
cd codewar
python3 -m venv env
source env/bin/activate
```
Install with pip:
```sh
pip install -e .
```

<a name="starting"></a>
### Start the game
Run the game:
```sh
./codewar/cwar_curses.py
```

<a name="contrib"></a>
## Contributing
Please take a look at [CONTRIBUTING.md](./CONTRIBUTING.md) if you're interested in helping!

<a name="license"></a>
## License
Distributed under the MIT License. See [LICENSE](./LICENSE) for more information.

<a name="contact"></a>
## Contact
Christian Gaida [@justplants_dev](https://t.me/justplants_dev) - Telegram 

Project Link: []()

<a name="acknowledgements"></a>
## Acknowledgements



<!-- MARKDOWN LINKS & IMAGES -->
[pipeline-badge]: https://gitlab.com/chgaida/codewar/badges/master/pipeline.svg
[pipeline-url]: https://gitlab.com/chgaida/codewar/-/commits/master
[pylint-badge]: https://gitlab.com/chgaida/codewar/-/jobs/artifacts/master/raw/public/badges/pylint.svg?job=pylint
[pylint-url]: https://gitlab.com/chgaida/codewar/-/commits/develop
[cover-badge]: https://gitlab.com/chgaida/codewar/badges/master/coverage.svg
[cover-url]: https://gitlab.com/chgaida/codewar/-/commits/master
[codestyle-shield]: https://img.shields.io/badge/code%20style-black-000000.svg
[codestyle-url]: https://github.com/psf/black
[python-version-shield]: https://img.shields.io/badge/python-3.7-blue
[python-version-url]: https://www.python.org/downloads/release/python-370/

