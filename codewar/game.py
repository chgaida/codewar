"""This modul contains the classes for the game C0D3 WAR."""


import random
from codewar.player import Player
from codewar.places import LOCATIONS
from codewar.events import Event
from codewar.events import _events


class Game:
    """This class represents the game."""

    _min_rounds = 3
    _lending_rate = 1.07
    _bank_interest = {"high": 1.05, "mid": 1.02, "low1": 1.005, "low2": 1.004}

    def __init__(self, max_rounds=30):
        """Initialize the game object here.

        Args:
            max_rounds (int): number of rounds the game will last

        """
        if max_rounds < self._min_rounds:
            max_rounds = self._min_rounds

        self._max_rounds = max_rounds
        self.game_round = 0  # stopped
        self.game_over = True

        self.player = None
        self._places = LOCATIONS

        # Start at a random place
        self.destination = self._places[random.choice(list(self._places))]

    def start(self, player_name=None):
        """Start a new game.

        'The sky above the port was the color of television, tuned to a dead channel'.

        """
        if not player_name:
            raise ValueError("You need a player to play the game.")
        self.player = Player(player_name)
        self.game_round = 1
        self.game_over = False

    def next(self, destination):
        """Game loop."""
        if destination not in self._places.keys():
            raise ValueError(f"{destination}: unknown destination.")
        if self._places[destination] == self.destination:
            raise ValueError(f"{destination}: still there.")
        if not self.player:
            raise ValueError(f"You need to start the game first.")

        self.destination = self._places[destination]

        self._update()

        if self.game_over:
            print("GAME OVER!")
            return

    def event(self, callback=None, win=None):
        """Event handler."""
        if callback:
            if self.destination.name == "Metro Holografix":
                new_event = Event(_events["hosaka"], callback=callback, win=win)
                message = new_event.event_handler(self.player)
            elif self.destination.name == "Zion Cluster":
                new_event = Event(_events["maelcum"], callback=callback, win=win)
                message = new_event.event_handler(self.player)
            else:
                message = None
                #new_event = Event(
                #    _events["turing_registry"], callback=callback, win=win
                #)
                #message = new_event.event_handler(self.player)
        else:
            message = self.destination.market.rnd_change_article_price()

        return message

    def _update(self):
        """Update the game variables."""
        self.game_round += 1
        self.game_over = self.game_round > self._max_rounds or self.game_round == 0
        self.destination.update_prices()

        # expand debts and bank interests
        self.player.debt = int(self.player.debt * self._lending_rate)
        credit = {"high": 50000, "mid": 100000, "low1": 500000, "low2": 999000000}
        for interest_rate, value in credit.items():
            if self.player._balance < value:
                self.player._balance = int(
                    self.player._balance * self._bank_interest[interest_rate]
                )
                break

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"(dest={self.destination.name},"
            f"round={self.game_round}, game over={self.game_over})"
        )
