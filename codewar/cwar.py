#!/usr/bin/env python


import pprint
from codewar.game import Game
from codewar.places import LOCATIONS


def jet(game):
    locations = LOCATIONS.copy()
    destinations = []
    # for k, v in locations.items():
    #    if v.name == game.destination.name:
    #        locations.pop(k)
    #        break
    # if game.destination.name != "The Sprawl":
    #    if game.destination.name != "Gentleman Loser":
    #        locations.pop("loser")
    #    if game.destination.name != "Metro Holografix":
    #        locations.pop("metro")
    # if game.destination.name != "Freeside":
    #    if game.destination.name != "Zion Cluster":
    #        locations.pop("zion")
    for i, (k, v) in enumerate(locations.items()):
        print(f"({i}): {v.name}")
        destinations.append(k)
    choice = -1
    while not choice in range(0, len(destinations)):
        choice = int(input("Where do you wanna travel? "))
    return destinations[choice]


def buy(game):
    items = []
    print("\n")
    for i, (item, price) in enumerate(game.destination.articles):
        print(f"({i}): {item} -> {price}$")
        article = (item, price)
        items.append(article)
    print("\n")
    try:
        item = int(input("What do you wanna buy? "))
        if item in range(0, len(items)):
            amount = int(input("How much? "))

            return (items[item][0], items[item][1], amount)
    except ValueError:
        print("Please enter a number.")


def sell(game):
    items = []
    print("\n")
    for i, (item, price) in enumerate(game.destination.articles):
        inv = game.player.inventory.get(item)
        stock = 0 if not inv else inv.stock
        print(f"({i}): {item} -> {price} ({stock})")
        article = (item, price)
        items.append(article)
    print("\n")
    try:
        item = int(input("What do you wanna sell? "))
        if item in range(0, len(items)):
            amount = int(input("How much? "))

            return (items[item][0], items[item][1], amount)
    except ValueError:
        print("Please enter a number.")


def main():
    game = Game(16)

    game.start("cri5h")
    print("The game started.")

    player = game.player

    p = pprint.PrettyPrinter(indent=2)

    while not game.game_over:
        try:
            print(f"\n{game}")
            print(f"{player}\n")
            print(f"\nLocal market:")
            print(f"{p.pprint(game.destination.articles)}\n")
            print(f"\nInventory:")
            print(f"{p.pprint(player.inventory)}\n")

            if game.destination.homebase:
                choice = "-"
                while choice != "":
                    choice = input("(p)ay debt, (g)oto bank? ")
                    if choice == "p":
                        amount = int(input("How much? "))
                        game.player.pay_back_debts(amount)
                    elif choice == "g":
                        choice = input("(d)eposit or (w)ithdraw? ")
                        amount = int(input("How much? "))
                        if choice == "d":
                            game.player.deposit(amount)
                        if choice == "w":
                            game.player.withdraw(amount)

            choice = input("(b)uy, (s)ell or (j)et? ")

            if choice == "b":
                # item = input("What do you wanna buy? ")
                # amount = int(input("How much? "))
                try:
                    item, price, amount = buy(game)
                    player.buy_item(item, amount, price)
                except TypeError:
                    print("Going on...")
            elif choice == "s":
                # item = input("What do you wanna sell? ")
                # amount = int(input("How much? "))
                try:
                    item, price, amount = sell(game)
                    player.sell_item(item, amount, price)
                except TypeError:
                    print("Going on...")
            elif choice == "j":
                dest = jet(game)
                game.next(dest)
        except ValueError as valerr:
            print(f"Error: {valerr}")


if __name__ == "__main__":
    main()
