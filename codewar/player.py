"""This module contains the player and inventory classes for the game C0D3 WAR."""


from dataclasses import dataclass


@dataclass
class InventoryItem:
    """Class for keeping track of an item in inventory."""

    name: str
    stock: int
    avg_price: int = 0
    special: bool = False


class Inventory:
    """Contains the various items of the player."""

    _min_size = 100

    def __init__(self, size=_min_size):
        """Initialize an empty inventory of a defined size.

        Args:
            size (int): The capacity of the inventory

        """
        if size < self._min_size:
            size = self._min_size
        self.__size = size

        self.inventory = dict()

    @property
    def total_space(self):
        """Inventory capacity."""
        return self.__size

    @property
    def free_space(self):
        """Space left in inventory."""
        used_space = sum([item.stock for item in self.inventory.values()])

        return self.__size - used_space

    def _add_space(self, amount):
        """Expand the carrying capacity."""
        self.__size += amount

    def _sub_space(self, amount):
        """Reduce the carrying capacity."""
        if self.__size - amount < 0:
            raise ValueError(f"Not enough space: {self.__size - amount}")

        self.__size -= amount

    def _in_stock(self, article):
        """Look if article is in the inventory.

        Args:
            article (str): The item we are looking for

        Returns:
            0 if not in stock, otherwise the amount in inventory

        """
        stock = 0

        if article in self.inventory.keys():
            stock = self.inventory[article].stock

        return stock

    def _add_item(self, article, amount, price=0, special=False):
        """Add an item to the inventory."""
        if amount <= 0:
            raise ValueError("Nothing to add.")

        item = self.inventory.get(
            article,
            InventoryItem(name=article, stock=0, avg_price=price, special=special),
        )

        stock = item.stock + amount
        avg_price = ((item.stock * item.avg_price) + (amount * price)) // stock

        self.inventory.update(
            {
                article: InventoryItem(
                    name=article, stock=stock, avg_price=avg_price, special=special
                )
            }
        )

    def _drop_item(self, article, amount):
        """Drop an item from the inventory."""
        if amount <= 0:
            raise ValueError(f"You can't drop nothing or less: {amount}")
        item = self.inventory.get(article, None)

        if not item:
            raise ValueError(f"Not in inventory: {article}")

        if item.stock < amount:
            raise ValueError("Not enough in inventory: {item.stock}")

        item.stock -= amount
        if item.stock == 0:
            self.inventory.pop(item.name)
        else:
            self.inventory.update({item.name: item})


class Player(Inventory):
    """This class represents the player."""

    _space = 100

    def __init__(self, name, money=2000, debt=5000):
        """Initialize the player.

        Args:
            name (str): Playername
            money (int): The player's starting capital
            debt (int): Starting debts with the loan shark

        """
        Inventory.__init__(self, size=self._space)
        self.name = name
        self._health = 100
        self._balance = 0
        self.money = money
        self.debt = debt
        self.destination = "chiba"

    @property
    def balance(self):
        """Return the players balance."""
        return self._balance

    def buy_item(self, item, amount, price, special=False):
        """Buy and add item to the inventory."""
        if self.money < amount * price:
            raise ValueError(f"Not enough money: {self.money}")
        if self.free_space < amount:
            raise ValueError(
                f"You can't buy more than you can carry: {self.free_space}"
            )

        self._add_item(item, amount, price, special)
        self.money -= amount * price

    def sell_item(self, article, amount, price):
        """Sell item from inventory."""
        stock = self._in_stock(article)

        if stock == 0:
            raise ValueError("Nothing to sell.")
        elif stock < amount:
            raise ValueError(f"{article}={stock}: not enough.")

        self._drop_item(article, amount)
        self.money += amount * price

    def jet(self, destination):
        """Travel to the next destination."""
        self.destination = destination

    def pay_back_debts(self, amount):
        """Pay back debts to the loan shark."""
        if amount > self.money:
            raise ValueError("You don't have enough money.")

        # Don't pay back more than the debt is
        if self.debt - amount < 0:
            amount = self.debt

        self.debt, self.money = self.debt - amount, self.money - amount

    def deposit(self, amount):
        """Deposit money into account."""
        if amount > self.money:
            raise ValueError("You don't have that much money.")
        self._balance, self.money = self._balance + amount, self.money - amount

    def withdraw(self, amount):
        """Withdraw funds from the account."""
        if amount > self._balance:
            raise ValueError("Not enough credit.")
        self.money, self._balance = self.money + amount, self._balance - amount

    def __repr__(self):
        return (
            f"{self.__class__.__name__}"
            f"(name={self.name}, money={self.money}, "
            f"debt={self.debt}, balance={self._balance}, "
            f"free space={self.free_space})"
        )
