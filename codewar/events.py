"""This module contains the events for the game C0D3 WAR."""


# from codewar.places import ITEMS


class Event:
    """This class ..."""

    def __init__(self, event, callback, win=None):
        self.location = event["location"]
        self.item = event["item"]["name"]
        self.price = event["item"]["price"]
        self.special = event["item"]["special"]
        self.text_stock = event["text_stock"]
        self.text_nistock = event["text_nistock"]
        self.text_ok = event["text_ok"]
        self.text_nok = event["text_nok"]
        self.question = event["question"]
        self.callback = callback
        # self.callback_answer = event["callback_answer"]
        self.title = self.location
        self.win = win

    def event_handler(self, player):
        """Receives and handles an event."""
        if self.item in player.inventory:
            self.callback(self.text_stock, title=self.title, win=self.win)
            callback_answer = "[ok]"
        else:
            answer = self.callback(
                self.text_nistock,
                title=self.title,
                question=self.question,
                win=self.win,
            )

            if answer:
                player.buy_item(self.item, 1, self.price, special=self.special)
                if self.item == "Backpack":
                    player._add_space(20)
                callback_answer = self.text_ok
            else:
                callback_answer = self.text_nok

        return callback_answer


_events = {
    "hosaka": {
        "location": "The shop of the Finn",
        "item": {
            "name": "Hosaka Ono-Sendai Cyberspace 7",
            # "price": 21500,
            "price": 21,
            "special": True,
        },
        "text_stock": (
            "The Finn: 'Hey cybercowboy, it's been a long \n"
            "           time since we met.'\n"
        ),
        "text_nistock": (
            "You are at the Finn's shop. The Finn offers you a\n"
            "Hosaka Ono-Sendai Cyberdeck 7 for $21500. Do you\n"
            "want to buy it?\n"
        ),
        "text_ok": "The Finn: 'Thank you.'\n",
        "text_nok": "The Finn: 'Never mind. Good bye.'\n",
        "question": "(y)es or (n)o?",
    },
    "maelcum": {
        "location": "At Zion Cluster",
        "item": {
            "name": "Backpack",
            # "price": 3500,
            "price": 35,
            "special": True,
        },
        "text_stock": ("Maelcum: 'Hey dude! What's going on?'\n"),
        "text_nistock": (
            "Maelcum: 'Hey dude, come over, I'll show you something.'\n"
            "Maelcum: 'Do you wanna buy this nice backpack?\n"
            "Maelcum: 'Only 3500 bucks for you.'\n"
        ),
        "text_ok": "Maelcum: 'Thank's brother!'\n",
        "text_nok": "Maelcum: 'Never mind bro. Peace out!'\n",
        "question": "(y)es or (n)o?",
    },
    "stranger": {
        "location": "In a back yard",
        "item": {
            "name": "Cassette",
            # "price": 7500,
            "price": 75,
            "special": True,
        },
        "text_stock": "Stanger: 'Hey guy, it's good to see you healthy again.'",
        "text_nistock": (
            "Stranger: 'Hey, psssst, console cowboy.'\n"
            "Stranger: 'I've got something you can't resist.'\n\n"
            "  The stranger shows you a cassette with an\n"
            "  inscription: KUANG GRADE MARK ELEVEN\n\n"
            "Stranger: 'I know this Icebreaker is worth 50000 bucks at least.'\n"
            "Stranger: 'And I know that you really want it.'\n"
            "Stranger: 'You look nice, only 30000 bucks for you.'\n"
            "Stranger: 'Shall we get into business?'"
        ),
        "text_ok": (
            "Stranger: 'You will not regret it! Time for a run in the Sprawl!'"
        ),
        "text_nok": "Stranger: 'Your decicion...'",
        "question": "(y)es or (n)o?",
    },
    "turing_registry": {
        "location": "",
        "item": {
            "name": "",
            # "price": 7500,
            "price": 75,
            "special": False,
        },
        "text_stock": ": '...'",
        "text_nistock": ("Turing Registry: ''\n"),
        "text_ok": "Turing Registry: ''",
        "text_nok": "Turing Registry: ''",
        "question": "(r)un or (p)ay?",
    },
    "dixie_flatline": {
        "location": "",
        "item": {
            "name": "",
            # "price": 7500,
            "price": 75,
            "special": False,
        },
        "text_stock": "",
        "text_nistock": ("The Dixie Flatline: ''\n"),
        "text_ok": "The Dixie Flatline: ''",
        "text_nok": "The Dixie Flatline: ''",
        "question": False,
    },
}
