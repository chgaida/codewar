#!/usr/bin/env python


"""The reference implementation of C0D3 WAR with curses ui."""


import curses
import getpass
import time
from codewar.game import Game
from codewar.places import LOCATIONS
from codewar.ui_curses import StatusBar, MessageBox


ERROR_COLOR = 1


def query_string(window, question):
    """Queries a string/a number.

    The query text is placed at the bottom of the window.

    """
    height, width = window.getmaxyx()
    space = width - len(question) - 2
    start_input = len(question) + 2

    window.addstr(height - 2, 1, question + " " * space)
    window.refresh()
    curses.echo()
    c = ""
    while not c.isdigit():
        c = window.getstr(height - 2, start_input, 5)
        window.addstr(height - 2, 1, question + " " * space)
        window.refresh()
    curses.noecho()

    return int(c)


def query_key(window, number_range):
    """Queries a single keystroke."""
    k = ""
    while not k.isdigit():
        k = window.getkey()

    if int(k) not in number_range:
        k = query_key(window, number_range)

    return int(k)


def market_pad(articles):
    """Display the articles."""
    pad = curses.newpad(12, 40)
    pad.box()
    pad.addstr(0, 2, "Market")
    
    for i, article in enumerate(articles.splitlines()):
        pad.addstr(i + 1, 1, article)

    pad.refresh(0, 0, 5, 5, 16, 44)


def market(game, highlight=None):
    """Fill the market with articles."""
    article_list = ""
    for i, (item, price) in enumerate(game.destination.articles):
        if i == highlight:
            article_list += f"-> ({i}) {item:<11} ${price:<5}\n"
        else:
            article_list += f"({i}) {item:<11} ${price:<5}\n"
    
    market_pad(article_list)


def inventory_pad(player):
    """Display the player's inventory."""
    pad = curses.newpad(15, 40)

    inv_list = []
    special_list = [item.name for item in player.inventory.values() if item.special]
    
    for item in player.inventory.values():
        if not item.special:
            inv_list.append(
                f"{item.name:<11} {item.stock:>4} pc. ${item.avg_price} avg."
            )

    pad.box()
    pad.addstr(0, 2, "Inventory")
    pad.addstr(0, 31, f"{player.free_space}/{player.total_space}")

    cur_line = 0
    for item in inv_list:
        pad.addstr(cur_line + 1, 1, item)
        cur_line += 1

    if special_list:
        pad.addstr(cur_line + 2, 1, "-- ")
        cur_line += 2
        for item in special_list:
            pad.addstr(cur_line + 1, 1, item)
            cur_line += 1

    pad.refresh(0, 0, 5, 50, 19, 100)


def menu_win():
    """Display the menu for buying, selling and traveling."""
    menu_str = "(b)uy, (s)ell or (j)et? "
    mwin = curses.newwin(3, 40, 17, 5)
    mwin.box()
    mwin.addstr(1, 1, menu_str)
    mwin.refresh()


def buy_win(game):
    """Let the player buy items."""
    buy_str = "What do you wanna buy? "
    bwin = curses.newwin(3, 40, 17, 5)
    bwin.box()
    bwin.addstr(1, 1, buy_str)
    bwin.refresh()

    items = []

    for item, price in game.destination.articles:
        article = (item, price)
        items.append(article)

    k = query_key(bwin, range(0, len(items)))
    market(game, k)
    c = query_string(bwin, "How much?")

    return (items[k][0], items[k][1], c)


def market_sell(game, highlight=None):
    """Display players stock in the market and highlight choice."""
    articles = ""
    items = []

    for i, (item, price) in enumerate(game.destination.articles):
        inv = game.player.inventory.get(item)
        stock = 0 if not inv else inv.stock
        if i == highlight:
            articles += f"-> ({i}) {item:<11} ${price:<5} ({stock})\n"
        else:
            articles += f"({i}) {item:<11} ${price:<5} ({stock})\n"

        items.append((item, price))

    return (articles, items)


def sell_win(game):
    """Let the player sell items."""
    swin = curses.newwin(3, 40, 17, 5)
    swin.box()

    articles, items = market_sell(game)
    market_pad(articles)

    swin.addstr(1, 1, "What do you wanna sell? ")
    swin.refresh()

    k = query_key(swin, range(0, len(items)))

    articles, items = market_sell(game, k)
    market_pad(articles)

    c = query_string(swin, "How much?")

    return (items[k][0], items[k][1], c)


def jet_win():
    """Display locations and enable the user to travel."""
    locations = LOCATIONS.copy()
    destinations = []
    dest_str = ""

    for i, (k, v) in enumerate(locations.items()):
        dest_str += f"({i}) {v.name}\n"
        destinations.append(k)

    jpad = curses.newpad(12, 40)
    jpad.attron(curses.color_pair(4))
    jpad.box()

    jpad.addstr(0, 2, "Travel")

    for i, dest in enumerate(dest_str.splitlines()):
        jpad.addstr(i + 1, 1, dest)

    jpad.refresh(0, 0, 5, 5, 17, 45)
    jpad.attroff(curses.color_pair(4))

    jwin = curses.newwin(3, 40, 17, 5)
    jwin.box()
    jwin.addstr(1, 1, "Where do you wanna travel? ")
    jwin.refresh()

    k = query_key(jwin, range(0, len(destinations)))

    return destinations[k]


def event_handler(message, **kwargs):
    """The callback function for general events."""
    query = kwargs.get("question", False)
    title = kwargs.get("title", None)
    win = kwargs.get("win")

    k, _ = win.display(message, title=title, question=query)

    if query:
        win.window.clear()
        win.window.refresh()

    return k == "y"


def events(game, message_box, destination, status_bar):
    """Call up the game events in squence."""
    event_msg = game.event()
    if event_msg is not None:
        message_box.display(event_msg)

    if destination == "nightcity":
        status_bar.update
        loan_shark(game.player, message_box)
    else:
        event_msg = game.event(callback=event_handler, win=message_box)
        if event_msg != "[ok]" and event_msg is not None:
            message_box.display(event_msg)


def loan_shark(player, message_box):
    """Meet the loan shark in Night City."""
    if player.debt > 0:
        text = (
            "The loan shark grins at you viciously.\n"
            "Loan shark: 'Wanna pay off your debts?'\n"
        )
        title = "In the loan shark's office"

        answer, window = message_box.display(text, title=title, question="(y)es or (n)o?")

        if answer == "y":
            amount = query_string(window, "Pay back:")
            player.pay_back_debts(amount)

        window.clear()
        window.refresh()


def game_over(status_bar, message_box, player):
    """Final settlement and game over screen."""
    status_bar.update

    value_inventory = sum(
        [item.stock * item.avg_price for item in player.inventory.values()]
    )
    money = (
        player.money + player.balance + value_inventory - player.debt
    )
    appendix = "You suck" if money < 0 else "Congratulations,"

    message_box.display(
        f"GAME OVER\nMoney: ${money}\n{appendix} {player.name}", center=True
    )

def init_curses(stdscr):
    """Initialize curses colors and some settings."""
    curses.start_color()
    curses.use_default_colors()
    curses.init_pair(1, curses.COLOR_WHITE, curses.COLOR_RED)
    curses.init_pair(2, curses.COLOR_GREEN, curses.COLOR_BLACK)
    curses.init_pair(3, curses.COLOR_BLACK, curses.COLOR_WHITE)
    curses.init_pair(4, curses.COLOR_YELLOW, curses.COLOR_BLACK)

    stdscr.clear()
    stdscr.box()


def main(stdscr):
    """Initilisation and main game loop."""
    init_curses(stdscr)
    username = getpass.getuser()

    game = Game()
    game.start(username)
    player = game.player
    # game._max_rounds = 15

    status_bar = StatusBar(stdscr, game)
    message_box = MessageBox(stdscr)

    while not game.game_over:
        try:
            status_bar.update
            inventory_pad(player)
            market(game)
            menu_win()

            k = ""
            while k not in "q b s j".split():
                k = stdscr.getkey()
                if k == "q":
                    game.game_over = True
                if k == "b":
                    item, price, amount = buy_win(game)
                    player.buy_item(item, amount, price)
                if k == "s":
                    item, price, amount = sell_win(game)
                    player.sell_item(item, amount, price)
                if k == "j":
                    dest = jet_win()
                    game.next(dest)
                    events(game, message_box, dest, status_bar)
        except Exception as err:
            message_box.display(str(err), text_color=ERROR_COLOR)
            stdscr.clear()

    game_over(status_bar, message_box, player)


curses.wrapper(main)
