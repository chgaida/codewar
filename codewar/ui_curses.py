"""UI classes for C0D3 WAR curses version."""


import curses


class StatusBar:
    """Status bar at the top of the main screen."""

    def __init__(self, stdscr, game):
        self._screen = stdscr
        self.screen_height, self.screen_width = self._screen.getmaxyx()
        self._game = game

    @property
    def update(self):
        """Print the current status values."""
        status_line = (
            f"Location: {self._game.destination.name} | "
            f"Wallet: ${self._game.player.money} | "
            f"Debt: ${self._game.player.debt} | "
            f"Bank: ${self._game.player.balance} | "
            f"Day: {self._game.game_round}/{self._game._max_rounds}"
        )
        space = self.screen_width - len(status_line) - 2
        start_space = len(status_line) + 1

        self._screen.attron(curses.color_pair(3))
        self._screen.addstr(1, 1, status_line, curses.A_REVERSE)
        self._screen.addstr(1, start_space, " " * space, curses.A_REVERSE)

        self._screen.attroff(curses.color_pair(3))
        self._screen.refresh()


class MessageBox:
    """A message box that is displayed in the center of the main screen."""

    def __init__(self, stdscr):
        self.screen_height, self.screen_width = stdscr.getmaxyx()
        self.window = curses.newwin(1, 1, 1, 1)

    @classmethod
    def box_size_yx(cls, message):
        """Calculates the message box size."""
        height = len(message.splitlines()) + 4
        width = max([len(l) for l in message.splitlines()]) + 2
        return height, width

    def pos_yx(self, message):
        """Calculates the y and x position of the box."""
        height, width = self.box_size_yx(message)
        y = int(self.screen_height / 2 - height)
        x = int(self.screen_width / 2 - width / 2)
        return y, x

    @classmethod
    def x_pos(cls, text, width, center):
        """Return the x position of the text."""
        if center:
            return int((width - len(text)) / 2)
        
        return 1

    def display(self, message, **kwargs):
        """Display a text message."""
        if not message:
            raise Exception("No message to display.")

        title = kwargs.get("title", "")
        center = kwargs.get("center", False)
        color = kwargs.get("text_color", 2)
        yesno = kwargs.get("question", False)

        height, width = self.box_size_yx(message)
        y, x = self.pos_yx(message)

        self.window.mvwin(y, x)
        self.window.resize(height, width)

        self.window.box()

        self.window.addstr(0, 2, title)

        self.window.attron(curses.color_pair(color))

        for i, msg in enumerate(message.splitlines()):
            self.window.addstr(i + 1, self.x_pos(msg, width, center), msg)

        self.window.attroff(curses.color_pair(color))

        text = yesno if yesno else "[ok]"
        self.window.addstr(height - 2, self.x_pos(text, width, True), text)

        self.window.refresh()

        key_pressed = self.window.getkey()

        if not yesno:
            self.window.clear()
            self.window.refresh()

        return key_pressed, self.window
