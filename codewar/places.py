"""This modul contains the places with the markets in the game."""


import random
from dataclasses import dataclass
from typing import List


@dataclass
class Article:
    """Class for keeping track of the marketplace articles."""

    name: str
    price_min: int
    price_max: int
    price: int = 0
    special: bool = False

    def __post_init__(self):
        self.price = self.random_price()

    def random_price(self):
        """Calculate a random price between min and max."""
        return random.randint(self.price_min, self.price_max)

    def __str__(self):
        return f"{self.name}: {self.price}"


@dataclass
class Market:
    """Class representing a market with a list of articles."""

    articles: List[Article]

    def rnd_change_article_price(self):
        """Randomly change an article price."""
        prices_rising = random.randint(0, 1)

        if prices_rising:
            multiplier = 7
            trend = "rising"
        else:
            multiplier = 0.3
            trend = "falling"

        if random.randint(0, 10) > 5:
            article_no = random.randint(0, len(self.articles) - 1)
            new_price = int(self.articles[article_no].price * multiplier)
            self.articles[article_no].price = new_price

            return f"The price for {self.articles[article_no].name} is {trend}!"


class Place:
    """This class represents a place in the game."""

    def __init__(self, name, market, homebase=False):
        """Init the place."""
        self.name = name
        self.market = market
        self.homebase = homebase
        self._articles = dict()

        for article in self.market.articles:
            self._articles[article.name] = article

    @property
    def articles(self):
        """Return list of article with prices."""
        return [(a.name, a.price) for a in self.market.articles]

    def update_prices(self):
        """Update the price of the articles in the local markt."""
        for article in self.market.articles:
            article.price = article.random_price()


ITEMS = {
    "icebreaker": Article("Icebreaker", 11000, 22000),
    "biosoft": Article("Biosoft", 6000, 13000),
    "cyberdeck": Article("Cyberdeck", 3000, 8000),
    "rom": Article("ROM", 1000, 4500),
    "implant": Article("Implant", 500, 2700),
    "software": Article("Software", 500, 2000),
    "dex": Article("Dex", 150, 750),
    "ketamine": Article("Ketamine", 80, 550),
    "derm": Article("Derm", 20, 350),
    "sim": Article("SimStim", 400, 6000),
    "backpack": Article("Backpack", 0, 0, special=True),
    "hosaka": Article("Hosaka Ono-Sendai Cyberspace 7", 0, 0, special=True),
}

LOCATIONS = {
    "sprawl": Place(
        "The Sprawl",
        Market(
            [
                ITEMS["icebreaker"],
                ITEMS["cyberdeck"],
                ITEMS["sim"],
                ITEMS["rom"],
                ITEMS["software"],
                ITEMS["dex"],
                ITEMS["ketamine"],
            ]
        ),
    ),
    "metro": Place(
        "Metro Holografix",
        Market([ITEMS["cyberdeck"], ITEMS["rom"], ITEMS["implant"], ITEMS["software"]]),
    ),
    "loser": Place(
        "Gentleman Loser", Market([ITEMS["software"], ITEMS["dex"], ITEMS["ketamine"]])
    ),
    "nightcity": Place(
        "Night City",
        Market(
            [
                ITEMS["icebreaker"],
                ITEMS["biosoft"],
                ITEMS["rom"],
                ITEMS["implant"],
                ITEMS["software"],
                ITEMS["dex"],
                ITEMS["ketamine"],
                ITEMS["derm"],
            ]
        ),
    ),
    "chiba": Place(
        "Chiba City",
        Market(
            [
                ITEMS["cyberdeck"],
                ITEMS["biosoft"],
                ITEMS["implant"],
                ITEMS["software"],
                ITEMS["derm"],
            ]
        ),
        homebase=True,
    ),
    "istanbul": Place(
        "Istanbul",
        Market(
            [
                ITEMS["biosoft"],
                ITEMS["implant"],
                ITEMS["software"],
                ITEMS["dex"],
                ITEMS["ketamine"],
                ITEMS["derm"],
            ]
        ),
    ),
    "freeside": Place(
        "Freeside",
        Market(
            [
                ITEMS["icebreaker"],
                ITEMS["biosoft"],
                ITEMS["dex"],
                ITEMS["ketamine"],
                ITEMS["derm"],
            ]
        ),
    ),
    "zion": Place(
        "Zion Cluster",
        Market(
            [
                ITEMS["icebreaker"],
                ITEMS["cyberdeck"],
                ITEMS["implant"],
                ITEMS["software"],
                ITEMS["dex"],
                ITEMS["derm"],
            ]
        ),
    ),
}
