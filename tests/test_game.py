"""Testmodule for the game class."""
# pylint: disable=W0212


import re
import random
import pytest
from codewar.game import Game, Player


def test_game_init_too_less_max_rounds():
    """Test if we play always minimum _min_rounds."""
    game = Game(2)
    assert game._max_rounds == game._min_rounds


def test_game_destination_not_none_on_init(game):
    """Test if the game destinations are initialized."""
    assert game.destination is not None


def test_next_with_unknown_destination(game):
    """Try to jet to an unknown location."""
    with pytest.raises(ValueError):
        game.next("northpole")


def test_next_with_the_same_location(game):
    """Test if we can travel without new destination."""
    dest = game.destination
    new_dest = random.choice(list(game._places))
    while game._places[new_dest] != dest:
        new_dest = random.choice(list(game._places))

    with pytest.raises(ValueError):
        game.next(new_dest)


def test_next_and_no_game_started(game):
    """Test if we can run the game without to start it."""
    dest = game.destination
    new_dest = random.choice(list(game._places))
    while game._places[new_dest] == dest:
        new_dest = random.choice(list(game._places))

    with pytest.raises(ValueError):
        game.next(new_dest)


def test_next_increments_game_round(game):
    """Test if the next round increments the game_round."""
    game.start("cri5h")
    dest = game.destination
    new_dest = random.choice(list(game._places))
    while game._places[new_dest] == dest:
        new_dest = random.choice(list(game._places))
    game.next(new_dest)
    assert game.game_round == 2


def test_start_without_player(game):
    """Try to start a game without player(-name)."""
    with pytest.raises(ValueError):
        game.start()


def test_start_if_new_player_is_created(game):
    """Test if a new player is created on a new game."""
    game.start("cri5h")
    assert isinstance(game.player, Player)


def test_start_first_game_round_is_one(game):
    """Test if the game_round is set to one after starting a game."""
    game.start("cri5h")
    assert game.game_round == 1


def test_next_game_over():
    """Test if last round stops the game."""
    game = Game(3)
    game.start("cri5h")
    game.game_round = 3
    dest = game.destination
    new_dest = random.choice(list(game._places))
    while game._places[new_dest] == dest:
        new_dest = random.choice(list(game._places))
    game.next(new_dest)
    assert game.game_over


def test_game_str_representation(game):
    """Test the string representation of the game object."""
    pattern = re.compile(r"Game\(dest=.*,round=0, game over=True\)")
    str_repr = game.__repr__()
    assert bool(pattern.search(str_repr))


def test_update_prices(game):
    """Test if the prices are updated in a new round."""
    game.start("cri5h")
    prices = [a.price for a in game.destination._articles.values()]
    game._update()
    new_prices = [a.price for a in game.destination._articles.values()]
    assert new_prices != prices
