"""Setup test suite."""


import pytest

from codewar.game import Game


@pytest.fixture
def game():
    """Create a new game."""
    return Game()
