"""Testmodule for the Inventory class."""
# pylint: disable=W0212

import pytest
from codewar.player import Inventory
from codewar.places import Article


def test_string_representation_of_article():
    """Test the string representation of an Article object."""
    article = Article("IceBreaker", 10, 10, 10)
    str_rep = str(article)
    assert str_rep == "IceBreaker: 10"


def test_capacity_after_init():
    """Test the default capacity of the inventory."""
    inv = Inventory()
    assert inv.total_space == 100


def test_capacity_min_size():
    """Test the minimum capacity of the inventory."""
    inv = Inventory(90)
    assert inv.total_space == 100


def test_add_space():
    """Test expanding the capacity."""
    inv = Inventory()
    inv._add_space(10)
    assert inv.total_space == 110


def test_sub_space():
    """Test reducing the space."""
    inv = Inventory(120)
    inv._sub_space(10)
    assert inv.total_space == 110


def test_sub_space_below_zero():
    """Test reducing the space below min."""
    inv = Inventory(100)
    with pytest.raises(ValueError):
        inv._sub_space(101)


def test_free_space_after_add_item():
    """Test the free space after we add an item."""
    inv = Inventory()
    inv._add_item("IceBreaker", 10)
    assert inv.free_space == 90


def test_if_item_not_in_stock():
    """Test if item is not in the inventory."""
    inv = Inventory()
    assert inv._in_stock("IceBreaker") == 0


def test_add_item_and_in_stock():
    """Test adding an item."""
    inv = Inventory()
    inv._add_item("IceBreaker", 1, price=1000)
    assert inv._in_stock("IceBreaker") == 1


def test_add_item_with_wrong_parameter_amount():
    """Test adding zero items."""
    inv = Inventory()
    with pytest.raises(ValueError):
        inv._add_item("IceBreaker", 0)


def test_add_item_with_negative_amount():
    """Test adding negative amount of items."""
    inv = Inventory()
    with pytest.raises(ValueError):
        inv._add_item("IceBreaker", -1)


def test_average_prize_after_add_item():
    """Test if the average price changes correct."""
    inv = Inventory()
    inv._add_item("IceBreaker", 1, price=1000)
    inv._add_item("IceBreaker", 1, price=2000)
    assert inv.inventory["IceBreaker"].avg_price == 1500


def test_stock_stock_after_add_item():
    """Test if the average price changes correct."""
    inv = Inventory()
    inv._add_item("IceBreaker", 1, price=1000)
    inv._add_item("IceBreaker", 1, price=1000)
    assert inv.inventory["IceBreaker"].stock == 2


def test_drop_item_with_zero_amount_of_items():
    """Test if we can drop zero items."""
    inv = Inventory()
    with pytest.raises(ValueError):
        inv._drop_item("IceBreaker", 0)


def test_drop_item_with_negative_amount_of_items():
    """Test if we can drop less than zero items."""
    inv = Inventory()
    with pytest.raises(ValueError):
        inv._drop_item("IceBreaker", -1)


def test_drop_item_that_not_exist():
    """Test dropping a non existant item."""
    inv = Inventory()
    with pytest.raises(ValueError):
        inv._drop_item("IceBreaker", 1)


def test_drop_item_more_than_in_stock():
    """Test dropping too much items."""
    inv = Inventory()
    inv._add_item("IceBreaker", 1)
    with pytest.raises(ValueError):
        inv._drop_item("IceBreaker", 2)


def test_drop_item_():
    """Test dropping an item."""
    inv = Inventory()
    inv._add_item("IceBreaker", 2)
    inv._drop_item("IceBreaker", 1)
    assert inv.inventory["IceBreaker"].stock == 1


def test_drop_item_clear_inventory():
    """Test if the item is deleted from inventory when the stock is zero."""
    inv = Inventory()
    inv._add_item("IceBreaker", 2)
    inv._drop_item("IceBreaker", 2)
    assert inv.inventory == {}
