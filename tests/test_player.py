"""Testmodule for the Player class."""
# pylint: disable=W0212


import pytest
from codewar.player import Player


def test_player_defaults():
    """Test the default values."""
    player = Player("cri5h")
    assert player._health == 100
    assert player.free_space == 100
    assert player._balance == 0
    assert player.debt == 5000
    assert player.money == 2000


def test_str_representation_of_player():
    """Test the string representation of a player object."""
    player = Player("cri5h")
    str_rep = "Player(name=cri5h, money=2000, debt=5000, balance=0, free space=100)"
    assert str(player) == str_rep


def test_buy_item_too_less_money():
    """Test if we can buy an item that's too expensive."""
    player = Player("cri5h")
    with pytest.raises(ValueError):
        player.buy_item("IceBreaker", 10, 1000)


def test_buy_item_no_space_left():
    """Test if we can buy more than we can carry."""
    player = Player("cri5h")
    player._sub_space(99)
    with pytest.raises(ValueError):
        player.buy_item("IceBreaker", 2, 1000)


def test_stock_after_buy_item():
    """Test if the inventory is correct after buying items."""
    player = Player("cri5h")
    player.buy_item("IceBreaker", 2, 1000)
    assert player.inventory["IceBreaker"].stock == 2


def test_money_after_buy_item():
    """Test if the inventory is correct after buying items."""
    player = Player("cri5h")
    player.buy_item("IceBreaker", 1, 1000)
    assert player.money == 1000


def test_sell_item_thats_not_in_stock():
    """Test if we can sell non existant items."""
    player = Player("cri5h")
    with pytest.raises(ValueError):
        player.sell_item("IceBreaker", 1, 1000)


def test_sell_item_more_than_in_stock():
    """Test if we can sell more items than we have."""
    player = Player("cri5h")
    player.buy_item("IceBreaker", 1, 1000)
    with pytest.raises(ValueError):
        player.sell_item("IceBreaker", 2, 1000)


def test_sell_item():
    """Sell one item."""
    player = Player("cri5h")
    player.buy_item("IceBreaker", 2, 1000)
    player.sell_item("IceBreaker", 1, 1000)
    assert player.inventory["IceBreaker"].stock == 1


def test_money_after_sell_item():
    """Test wallet after selling items."""
    player = Player("cri5h")
    player._add_item("IceBreaker", 2, 1000)
    player.sell_item("IceBreaker", 1, 1000)
    assert player.money == 3000


# Add test for existing destination


def test_destination():
    """Test if we can change destination."""
    player = Player("cri5h")
    player.jet("Night City")
    assert player.destination == "Night City"


def test_pay_back_depts_not_enough_money():
    """Test if we can pay back more than we have."""
    player = Player("cri5h")
    with pytest.raises(ValueError):
        player.pay_back_debts(2100)


def test_pay_back_depts_too_much_money():
    """Test if we can pay back too much."""
    player = Player("cri5h")
    player.debt = 1000
    player.pay_back_debts(2000)
    assert player.money == 1000


def test_depts_after_pay_back_depts():
    """Test pay back the depts."""
    player = Player("cri5h")
    player.pay_back_debts(2000)
    # debts are 5000
    assert player.debt == 3000


def test_money_after_pay_back_depts():
    """Test our wallet after pay back the depts."""
    player = Player("cri5h")
    player.pay_back_debts(1500)
    assert player.money == 500


def test_deposit_not_enough_money():
    """Test if we can deposit more than we have."""
    player = Player("cri5h")
    with pytest.raises(ValueError):
        player.deposit(2100)


def test_money_after_deposit():
    """Test our wallet after deposit."""
    player = Player("cri5h")
    player.deposit(1500)
    assert player.money == 500


def test_balance_after_deposit():
    """Test our balance after deposit."""
    player = Player("cri5h")
    player.deposit(1500)
    assert player._balance == 1500


def test_withdraw_not_enough_money():
    """Test if we can withdraw more than we have."""
    player = Player("cri5h")
    with pytest.raises(ValueError):
        player.withdraw(100)


def test_money_after_withdraw():
    """Test our wallet after deposit."""
    player = Player("cri5h")
    player._balance = 100
    player.withdraw(100)
    assert player.money == 2100


def test_balance_after_withdraw():
    """Test our balance after deposit."""
    player = Player("cri5h")
    player._balance = 1000
    player.withdraw(500)
    assert player._balance == 500
